#pragma once

enum TraceType_t : int
{
	TRACE_EVERYTHING = 0,
	TRACE_WORLD_ONLY,			   // NOTE: This does *not* test static props!!!
	TRACE_ENTITIES_ONLY,		   // NOTE: This version will *not* test static props
	TRACE_EVERYTHING_FILTER_PROPS, // NOTE: This version will pass the IHandleEntity for props through the filter, unlike all other filters
};