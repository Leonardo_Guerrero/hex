#include "globalvars.h"
#include "netvars.h"

namespace globalvars
{
	
	DWORD m_fFlags;
	DWORD m_lifeState;
	DWORD m_vecViewOffset;
	DWORD m_vecOrigin;
	DWORD m_hActiveWeapon;
	DWORD m_flNextPrimaryAttack;
	DWORD m_nTickBase;

	void init()
	{
		g_pNetVars = new CNetVars();
		m_fFlags = (DWORD)g_pNetVars->GetOffset("DT_BasePlayer", "m_fFlags");
		m_lifeState = (DWORD)g_pNetVars->GetOffset("DT_BasePlayer", "m_lifeState");
		m_vecViewOffset = (DWORD)g_pNetVars->GetOffset("DT_BasePlayer", "m_vecViewOffset[0]");
		m_vecOrigin = (DWORD)g_pNetVars->GetOffset("DT_BaseEntity", "m_vecOrigin");
		m_hActiveWeapon = (DWORD)g_pNetVars->GetOffset("DT_BaseCombatCharacter", "m_hActiveWeapon");
		m_flNextPrimaryAttack = (DWORD)g_pNetVars->GetOffset("DT_BaseCombatWeapon", "m_flNextPrimaryAttack");
		m_nTickBase = (DWORD)g_pNetVars->GetOffset("DT_BasePlayer", "m_nTickBase");
	}
}