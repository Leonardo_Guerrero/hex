#pragma once

#include "cbasetrace.h"
#include "csurface_t.h"
#include "c_baseentity.h"

class CGameTrace : public CBaseTrace
{
public:
	float fractionleftsolid;
	csurface_t surface;
	int hitGroup;
	short physicsBone;
	C_BaseEntity* m_pEnt;
	int hitbox;
	bool IsVisible()
	{
		return fraction > 0.97f;
	}
};

typedef CGameTrace trace_t;