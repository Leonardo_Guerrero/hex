#pragma once

#include "c_baseentity.h"
#include "cusercmd.h"
#include "log_info.h"

namespace globals
{
	extern C_BaseEntity* m_local;
	extern CUserCmd* m_cmd;
	extern std::unique_ptr<log_info> log;

	extern std::string transmessage;
	extern std::vector<std::string> msg_holder;
}