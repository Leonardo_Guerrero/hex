#pragma once

#include "general_includes.h"
#include "vector.h"

class CUserCmd
{
public:
	virtual ~CUserCmd() {}; //Destructor 0

	// For matching server and client commands for debugging
	int command_number; //4

	// the tick the client created this command
	int tick_count; //8

	// Player instantaneous view angles.
	Vector viewangles; //C

	// Intended velocities
	//	forward velocity.
	float forwardmove; //18

	//  sideways velocity.
	float sidemove; //1C

	//  upward velocity.
	float upmove; //20

	// Attack button states
	int	buttons; //24

	// Impulse command issued.
	byte impulse; //28

	// Current weapon id
	int weaponselect; //2C
	int weaponsubtype; //30

	// For shared random functions
	int random_seed; //34

	// mouse accum in x from create move
	short mousedx; //38

	// mouse accum in y from create move
	short mousedy; //3A

	// Client only, tracks whether we've predicted this command at least once
	bool hasbeenpredicted; //3C;
};