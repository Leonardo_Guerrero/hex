#pragma once

#include "vector.h"

struct Ray_t
{
	Vector m_Start;
	float w1;
	Vector m_Delta;
	float w2;
	Vector m_StartOffset;
	float w3;
	Vector m_Extents;
	float w4;

	bool m_IsRay;
	bool m_IsSwept;

	void Init(Vector const& start, Vector const& end)
	{
		//VectorSubtract( end, start, m_Delta );

		m_Delta = end - start;

		m_IsSwept = (m_Delta.LengthSqr() != 0);

		VectorClear(m_Extents);
		m_IsRay = true;

		VectorClear(m_StartOffset);
		VectorCopy(start, m_Start);
	}

	void Init(Vector const& start, Vector const& end, Vector const& mins, Vector const& maxs)
	{
		//VectorSubtract( end, start, m_Delta );

		m_Delta = end - start;

		m_IsSwept = (m_Delta.LengthSqr() != 0);

		//VectorSubtract( maxs, mins, m_Extents );

		m_Extents = maxs - mins;

		m_Extents *= 0.5f;
		m_IsRay = (m_Extents.LengthSqr() < 1e-6);

		//VectorAdd( mins, maxs, m_StartOffset );

		m_StartOffset = mins + maxs;

		m_StartOffset *= 0.5f;
		//VectorAdd( start, m_StartOffset, m_Start );
		m_Start = start + m_StartOffset;

		m_StartOffset *= -1.f;
	}
};