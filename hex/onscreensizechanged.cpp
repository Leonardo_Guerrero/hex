#include "onscreensizechanged.h"
#include "interfaces.h"
#include "drawing.h"
#include "globals.h"

namespace onscreensizechanged
{
	OnScreenSizeChangedFn oOnScreenSizeChanged;

	void __stdcall Hooked_OnScreenSizeChanged(int oldWidth, int oldHeight)
	{
		oOnScreenSizeChanged(interfaces::p_surface, oldWidth, oldHeight);
		
		/* Reinitialize our drawing stuff */
		drawing::init();

		globals::log->add_log_messasge(Color(0, 255, 0), "Screensize changed, re-adjusted isurface.");
	}
}