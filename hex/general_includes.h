#pragma once

#pragma warning( disable : 4244 )
#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <emmintrin.h>
#include <Psapi.h>
#include <cstdlib>
#include <xtgmath.h>
#include <unordered_map>
#include <memory>
#include <utility>
#include <errno.h>
#include <stdlib.h>
#include <thread>
#include <future>
#include <chrono>
#include <mutex>