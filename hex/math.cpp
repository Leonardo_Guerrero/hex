#include "math.h"
#include "interfaces.h"
#include "screen_size.h"
#include "mouse.h"

namespace math
{

	float distanceMeters(Vector v1, Vector v2)
	{
		float dist = sqrt(v1.DistToSqr(v2));
		return dist * 0.01905f;
	}

	namespace matrix
	{
		bool WorldToScreen(Vector& vOrigin, Vector& vScreen)
		{
			const matrix3x4& worldToScreen = interfaces::p_engine->WorldToScreenMatrix();
			float w = worldToScreen[3][0] * vOrigin[0] + worldToScreen[3][1] * vOrigin[1] + worldToScreen[3][2] * vOrigin[2] + worldToScreen[3][3]; //Calculate the angle in compareson to the player's camera.
			vScreen.z = 0; //Screen doesn't have a 3rd dimension.
			interfaces::p_engine->GetScreenSize(screen::ScrW, screen::ScrH);

			if (w > 0.001) //If the object is within view.
			{
				float fl1DBw = 1 / w; //Divide 1 by the angle.
				vScreen.x = (screen::ScrW / 2) + (0.5 * ((worldToScreen[0][0] * vOrigin[0] + worldToScreen[0][1] * vOrigin[1] + worldToScreen[0][2] * vOrigin[2] + worldToScreen[0][3]) * fl1DBw) * screen::ScrW + 0.5); //Get the X dimension and push it in to the Vector.
				vScreen.y = (screen::ScrH / 2) - (0.5 * ((worldToScreen[1][0] * vOrigin[0] + worldToScreen[1][1] * vOrigin[1] + worldToScreen[1][2] * vOrigin[2] + worldToScreen[1][3]) * fl1DBw) * screen::ScrH + 0.5); //Get the Y dimension and push it in to the Vector.
				return true;
			}

			return false;
		}
	}

	namespace trig
	{
		void SinCos(float radians, float* sine, float* cosine) {
			*sine = sin(radians);
			*cosine = cos(radians);
		}

		void AngleVectors(const Vector& angles, Vector* forward)
		{
			float sp, sy, cp, cy;

			SinCos(DEG2RAD(angles[YAW]), &sy, &cy);
			SinCos(DEG2RAD(angles[PITCH]), &sp, &cp);

			forward->x = cp * cy;
			forward->y = cp * sy;
			forward->z = -sp;
		}
	}
}