#include "dispatchusermessage.h"
#include "globals.h"
#include "utils.h"

#include "nlohmann/json.hpp"
#include <wininet.h>
#pragma comment(lib, "wininet.lib")
#include "translator.h"

std::string findlang(const std::string& text)
{
	using json = nlohmann::json;
	const std::string url = "https://translate.yandex.net/api/v1.5/tr/detect";
	const std::string key = "trnsl.1.1.20191022T053454Z.48addd19cae14adf.5e541e8704c8abc207abd615968dcdb2f79ff02d";

	auto _url = url + std::string("?key=") + key;
	_url += std::string("&text=") + text;
	_url += std::string("&hint=") + "en"; // Add some bullshit language

	auto connect = InternetOpenA("GoogleChrome", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);

	if (connect) {

		auto address = InternetOpenUrlA(connect, _url.c_str(), NULL, 0, INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_KEEP_CONNECTION, 0);
		if (address) {
			char data_received[1024];
			DWORD number_of_bytes_read = 0;

			if (InternetReadFile(address, data_received, 1024, &number_of_bytes_read) && number_of_bytes_read) {
				std::string output(data_received);
				InternetCloseHandle(address);
				InternetCloseHandle(connect);

				std::size_t posStart = output.find("code");
				std::size_t posEnd = output.find("/>");
				int size = posEnd - posStart;

				std::string language = output.substr(posStart + 17, size - 19);
				return language;

			}
		}
		else {
			InternetCloseHandle(connect);
		}
	}

	return std::string();
}

std::string trans(const std::string& text)
{
	using json = nlohmann::json;
	const std::string url = "https://translate.yandex.net/api/v1.5/tr.json/translate";
	const std::string key = "trnsl.1.1.20191022T053454Z.48addd19cae14adf.5e541e8704c8abc207abd615968dcdb2f79ff02d";

	auto _url = url + std::string("?key=") + key;
	_url += std::string("&text=") + text;
	_url += std::string("&lang=") + findlang(text) + "-en";

	auto connect = InternetOpenA("GoogleChrome", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);

	if (connect) {

		auto address = InternetOpenUrlA(connect, _url.c_str(), NULL, 0, INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_KEEP_CONNECTION, 0);
		if (address) {
			char data_received[1024];
			DWORD number_of_bytes_read = 0;

			if (InternetReadFile(address, data_received, 1024, &number_of_bytes_read) && number_of_bytes_read) {
				std::string output(data_received, number_of_bytes_read);
				InternetCloseHandle(address);
				InternetCloseHandle(connect);

				auto json = json::parse(output.c_str());
				//std::cout << json.dump() << std::endl;
				auto code = json["code"].get<int>();
				if (code == 200)
					return json["text"][0].get<std::string>();
			}
		}
		else {
			InternetCloseHandle(connect);
		}
	}

	return std::string();
}

namespace dispatchusermessage
{
	DispatchUserMessageFn oDispatchUserMessage;

	/* https://github.com/nullworks/cathook/blob/4f731e24c22142f5f3a97bfb838c92aa53c3be64/src/hooks/DispatchUserMessage.cpp#L83 */

	bool should_translate = false;

	bool __fastcall Hooked_DispatchUserMessage(void* ptr, int edx, int type, bf_read& msg_data)
	{
		
		int s;
		const char* buf_data = reinterpret_cast<const char*>(msg_data.m_pData);
		std::string data;

		switch (type)
		{
			case 4:
			{
				s = msg_data.GetNumBytesLeft();
				if (s >= 256)
					break;

				for (int i = 0; i < s; i++)
					data.push_back(buf_data[i]);


				const char* p = data.c_str() + 2;
				std::string event(p), name((p += event.size() + 1)), message(p + name.size() + 1);

				/* Only get messages that are from all chat */
				if (event.find("TF_Chat") == 0)
				{
					if (message.find("!ton") == 0)
						should_translate = !should_translate;

					if (should_translate)
					{
						if (message.find("!ton") == 0)
							return "";

						std::cout << "should_translate: on" << std::endl;

						globals::transmessage = message;
					}
				}
			}
		}

		return oDispatchUserMessage(ptr, type, msg_data);
	}
}