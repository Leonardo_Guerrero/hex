#pragma once

#include "itracefilter.h"

class CBaseFilter : public ITraceFilter
{
public:
	CBaseFilter() : pSelf(nullptr), pIgnore(nullptr) {}

	virtual bool ShouldHitEntity(C_BaseEntity* pEntity, int contentsMask)
	{
		return !(pEntity == pSkip);
	}

	virtual TraceType_t GetTraceType()
	{
		return TRACE_EVERYTHING;
	}
	void* pSkip;

	// Setup the filter
	inline void SetIgnoreSelf(C_BaseEntity* pSelf) { this->pSelf = pSelf; }
	inline void SetIgnoreEntity(C_BaseEntity* pIgnore) { this->pIgnore = pIgnore; }

protected:
	//void Debug( CBaseEntity* pEnt );

protected:
	C_BaseEntity* pSelf;
	C_BaseEntity* pIgnore;
};