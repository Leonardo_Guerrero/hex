#include "menu_input.h"

namespace menu_input
{

	fgui::state get_key_state(const fgui::key& key)
	{
		return interfaces::p_inputsystem->IsButtonDown(key);
	}

	fgui::delta get_scroll_delta()
	{
		return interfaces::p_inputsystem->GetAnalogDelta(analog_code_t::MOUSE_WHEEL);
	}

	fgui::point get_mouse_position()
	{
		static fgui::point temporary_point = { 0, 0 };
		interfaces::p_surface->SurfaceGetCursorPos(temporary_point.x, temporary_point.y);

		return temporary_point;
	}

	void init()
	{
		fgui::input.get_key_state = get_key_state;
		fgui::input.get_scroll_delta = get_scroll_delta;
		fgui::input.get_mouse_position = get_mouse_position;
	}
}