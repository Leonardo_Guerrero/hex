#pragma once

#include "general_includes.h"
#include "cusercmd.h"

namespace createmove
{
	/* Typedef */
	typedef bool(__stdcall* CreateMoveFn)(float, CUserCmd*);

	/* Original return */
	extern CreateMoveFn oCreateMove;

	/* Hooked function */
	bool __stdcall Hooked_CreateMove(float flInputSampleTime, CUserCmd* UserCmd);
}