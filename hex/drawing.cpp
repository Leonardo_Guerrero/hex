#include "drawing.h"

namespace drawing
{
	HFont m_menufont;
	HFont m_espfont;

	void init()
	{
		fgui::render.create_font = drawing::menu::create_font;
		fgui::render.get_screen_size = drawing::menu::get_screen_size;
		fgui::render.get_text_size = drawing::menu::get_text_size;
		fgui::render.rect = drawing::menu::draw_rect;
		fgui::render.outline = drawing::menu::draw_outlined_rectangle;
		fgui::render.line = drawing::menu::draw_line;
		fgui::render.text = drawing::menu::draw_string;
		fgui::render.colored_gradient = drawing::menu::draw_colored_gradient;
		fgui::render.clip_rect = drawing::menu::draw_clip_rect;

		interfaces::p_engine->GetScreenSize(screen::ScrW, screen::ScrH);

		m_menufont = interfaces::p_surface->CreateFont();
		interfaces::p_surface->SetFontGlyphSet(m_menufont, "Smallest Pixel-7", 11, 400, 0, 0, FONTFLAG_DROPSHADOW);

		m_espfont = interfaces::p_surface->CreateFont();
		interfaces::p_surface->SetFontGlyphSet(m_espfont, "04b03", 12, FW_DONTCARE, NULL, NULL, FONTFLAG_OUTLINE);
	}
	
	/* Regular string output with multiple arguments, used for things like visuals */
	void draw_string(HFont font, int x, int y, Color color, const char* text, ...)
	{
		char buf[1024];
		wchar_t wbuf[1024];

		va_list vlist;
		va_start(vlist, text);
		vsprintf(buf, text, vlist);
		va_end(vlist);

		MultiByteToWideChar(CP_UTF8, 0, buf, 256, wbuf, 256);

		interfaces::p_surface->DrawSetTextColor(color.r, color.g, color.b, color.a);
		interfaces::p_surface->DrawSetTextFont(font);
		interfaces::p_surface->DrawSetTextPos(x, y);
		interfaces::p_surface->DrawPrintText(wbuf, wcslen(wbuf));
	}

	void draw_rect(int x, int y, int x2, int y2, Color c)
	{
		interfaces::p_surface->DrawSetColor(c.r, c.g, c.b, c.a);
		interfaces::p_surface->DrawFilledRect(x, y, x + x2, y + y2);
	}

	void draw_outlined_rectangle(int x, int y, int x2, int y2, Color c)
	{
		interfaces::p_surface->DrawSetColor(c.r, c.g, c.b, c.a);
		interfaces::p_surface->DrawOutlinedRect(x, y, x + x2, y + y2);
	}

	void draw_line(int x, int y, int x2, int y2, Color c)
	{
		interfaces::p_surface->DrawSetColor(c.r, c.g, c.b, c.a);
		interfaces::p_surface->DrawLine(x, y, x2, y2);
	}

	/* Used to get visual and other things that are not part of menu size */
	void get_text_size(HFont font, int& w, int& h, const char* message, ...)
	{
		char buf[1024];
		wchar_t wbuf[1024];

		va_list vlist;
		va_start(vlist, message);
		vsprintf(buf, message, vlist);
		va_end(vlist);

		MultiByteToWideChar(CP_UTF8, 0, buf, 256, wbuf, 256);
		interfaces::p_surface->GetTextSize(font, wbuf, w, h);
	}

	namespace menu
	{
		void create_font(fgui::font& font, const std::string_view family, int size, int flags, bool bold)
		{
			font = interfaces::p_surface->CreateFont();
			interfaces::p_surface->SetFontGlyphSet(font, family.data(), size, bold ? 800 : 0, 0, 0, flags);
		}

		void get_screen_size(int& width, int& height)
		{
			interfaces::p_engine->GetScreenSize(width, height);
		}

		fgui::dimension get_text_size(const fgui::font& font, const std::string_view text)
		{
			fgui::dimension temp = { 0,0 };

			interfaces::p_surface->GetTextSize(font, std::wstring(text.begin(), text.end()).data(), temp.width, temp.height);

			return temp;
		}

		void draw_rect(int x, int y, int width, int height, fgui::color color)
		{
			interfaces::p_surface->DrawSetColor(color.m_red, color.m_green, color.m_blue, color.m_alpha);
			interfaces::p_surface->DrawFilledRect(x, y, x + width, y + height);
		}

		void draw_outlined_rectangle(int x, int y, int width, int height, fgui::color color)
		{
			interfaces::p_surface->DrawSetColor(color.m_red, color.m_green, color.m_blue, color.m_alpha);
			interfaces::p_surface->DrawOutlinedRect(x, y, x + width, y + height);
		}

		void draw_line(int x, int y, int x2, int y2, fgui::color color)
		{
			interfaces::p_surface->DrawSetColor(color.m_red, color.m_green, color.m_blue, color.m_alpha);
			interfaces::p_surface->DrawLine(x, y, x2, y2);
		}

		void draw_string(int x, int y, fgui::color color, fgui::font font, const std::string_view text)
		{
			const auto converted_text = std::wstring(text.begin(), text.end());

			interfaces::p_surface->DrawSetColor(color.m_red, color.m_green, color.m_blue, color.m_alpha);
			interfaces::p_surface->DrawSetTextFont(font);
			interfaces::p_surface->DrawSetTextPos(x, y);
			interfaces::p_surface->DrawPrintText(converted_text.c_str(), wcslen(converted_text.c_str()));
		}

		void draw_colored_gradient(int x, int y, int width, int height, fgui::color color1, fgui::color color2, bool is_horizontal)
		{
			if (is_horizontal)
			{ // horizontal

				draw_rect(x, y, width, height, color1);

				unsigned char first = color2.m_red;
				unsigned char second = color2.m_green;
				unsigned char third = color2.m_blue;

				for (int i = 0; i < width; i++)
				{
					float fi = i, fw = height;
					float a = fi / fw;
					unsigned int ia = a * 255;
					draw_rect(x + i, y, 1, height, fgui::color(first, second, third, ia));
				}
			}

			else
			{ // vertical

				draw_rect(x, y, width, height, color1);

				unsigned char first = color2.m_red;
				unsigned char second = color2.m_green;
				unsigned char third = color2.m_blue;

				for (int i = 0; i < height; i++)
				{

					float fi = i, fh = height;
					float a = fi / fh;
					unsigned int ia = a * 255;
					draw_rect(x, y + i, width, 1, fgui::color(first, second, third, ia));
				}
			}
		}

		void draw_clip_rect(int x, int y, int width, int height)
		{
			fgui::rect viewport = { x, y, (x + width), (y + height) };

			interfaces::p_surface->SetClippingRect(viewport.left, viewport.top, viewport.right, viewport.bottom);
		}
	}
};