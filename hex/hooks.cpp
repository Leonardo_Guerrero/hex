#include "hooks.h"
#include "paint.h"
#include "createmove.h"
#include "onscreensizechanged.h"
#include "dispatchusermessage.h"
#include "detours_includes.h"
#include "utils.h"
#include "globals.h"
#include "translator.h"

namespace hooks
{
	CVMTHook* Paint;
	CVMTHook* CreateMove;
	CVMTHook* OnScreenSizeChanged;
	CVMTHook* DispatchUserMessage;

	void init()
	{
		/* Drawing hook */
		Paint = new CVMTHook((DWORD**)interfaces::p_enginevgui);
		paint::oPaint = (paint::PaintFn)Paint->HookMethod((DWORD)paint::Hooked_Paint, 13);

		/* Movement hook, anything aimbot, bhop related */
		CreateMove = new CVMTHook((DWORD**)interfaces::p_clientmode);
		createmove::oCreateMove = (createmove::CreateMoveFn)CreateMove->HookMethod((DWORD)createmove::Hooked_CreateMove, 21);

		/* Screenchange hook */
		OnScreenSizeChanged = new CVMTHook((DWORD**)interfaces::p_surface);
		onscreensizechanged::oOnScreenSizeChanged = (onscreensizechanged::OnScreenSizeChangedFn)OnScreenSizeChanged->HookMethod((DWORD)onscreensizechanged::Hooked_OnScreenSizeChanged, 111);

		/* User messages hook */
		DispatchUserMessage = new CVMTHook((DWORD**)interfaces::p_client);
		dispatchusermessage::oDispatchUserMessage = (dispatchusermessage::DispatchUserMessageFn)DispatchUserMessage->HookMethod((DWORD)dispatchusermessage::Hooked_DispatchUserMessage, 36);	
	}

	void remove(HMODULE dll)
	{
		while (true)
		{
			if (GetAsyncKeyState(VK_END) & 1)
			{
				FreeConsole();
				Paint->UnHook();
				CreateMove->UnHook();
				OnScreenSizeChanged->UnHook();
				DispatchUserMessage->UnHook();
				Sleep(500);
				FreeLibraryAndExitThread(dll, 0);
			}
		}
	}
}