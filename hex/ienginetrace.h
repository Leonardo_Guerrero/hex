#pragma once

#include "ray_t.h"
#include "itracefilter.h"
#include "cgametrace.h"

class IEngineTrace
{
public:
	void TraceRay(const Ray_t& ray, unsigned int fMask, ITraceFilter* pTraceFilter, trace_t* pTrace)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, const Ray_t&, unsigned int, ITraceFilter*, trace_t*);
		return getvfunc<OriginalFn>(this, 4)(this, ray, fMask, pTraceFilter, pTrace);
	}
};