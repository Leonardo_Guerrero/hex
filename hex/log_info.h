#pragma once

#include "general_includes.h"
#include "interfaces.h"
#include "drawing.h"

struct info
{
	info(const char* text, float time, Color col)
	{
		this->txt = text;
		this->time = time;
		this->col = col;
	}

	const char* txt;
	float time;
	Color col;
};

class log_info
{
private:


public:
	std::vector<info> info_list;
	void draw_log_info();

	void add_log_messasge(Color col, const char* message);
};