#pragma once

#include "interfaces.h"

namespace hacks
{
	bool valid_entity(C_BaseEntity* pEntity);

	namespace demoknight
	{
		void demoknight_attack();
	}
}