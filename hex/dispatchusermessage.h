#pragma once
#include "bf_read.h"

namespace dispatchusermessage
{
	/* Typedef */
	typedef bool(__thiscall* DispatchUserMessageFn)(void*, int, bf_read&);

	/* Original return */
	extern DispatchUserMessageFn oDispatchUserMessage;

	/* Hooked function */
	bool __fastcall Hooked_DispatchUserMessage(void* ptr, int edx, int type, bf_read& msg_data);
}