#include "translator.h"
#include "nlohmann/json.hpp"
#include "globals.h"

namespace translator
{
	void hexchar(unsigned char c, unsigned char& hex1, unsigned char& hex2)
	{
		hex1 = c / 16;
		hex2 = c % 16;
		hex1 += hex1 <= 9 ? '0' : 'a' - 10;
		hex2 += hex2 <= 9 ? '0' : 'a' - 10;
	}

	std::string urlencode(std::string s)
	{
		const char* str = s.c_str();
		std::vector<char> v(s.size());
		v.clear();
		for (size_t i = 0, l = s.size(); i < l; i++)
		{
			char c = str[i];
			if ((c >= '0' && c <= '9') ||
				(c >= 'a' && c <= 'z') ||
				(c >= 'A' && c <= 'Z') ||
				c == '-' || c == '_' || c == '.' || c == '!' || c == '~' ||
				c == '*' || c == '\'' || c == '(' || c == ')')
			{
				v.push_back(c);
			}
			else if (c == ' ')
			{
				v.push_back('+');
			}
			else
			{
				v.push_back('%');
				unsigned char d1, d2;
				hexchar(c, d1, d2);
				v.push_back(d1);
				v.push_back(d2);
			}
		}

		return std::string(v.cbegin(), v.cend());
	}

	std::string get_translation_language(const std::string& text)
	{
		const std::string url = "https://translate.yandex.net/api/v1.5/tr.json/detect";
		const std::string key = "trnsl.1.1.20191022T053454Z.48addd19cae14adf.5e541e8704c8abc207abd615968dcdb2f79ff02d";

		auto _url = url + std::string("?key=") + key;
		_url += std::string("&text=") + urlencode(text);

		CURL* curl = curl_easy_init();

		// Set remote URL.
		curl_easy_setopt(curl, CURLOPT_URL, _url.c_str());

		// Don't bother trying IPv6, which would increase DNS resolution time.
		curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

		// Don't wait forever, time out after 10 seconds.
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

		// Follow HTTP redirects if necessary.
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

		// Response information.
		long httpCode(0);
		std::unique_ptr<std::string> httpData(new std::string());

		// Hook up data handling function.
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

		// Hook up data container (will be passed as the last parameter to the
		// callback handling function).  Can be any pointer type, since it will
		// internally be passed as a void pointer.
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());

		// Run our HTTP GET command, capture the HTTP response code, and clean up.
		curl_easy_perform(curl);
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
		curl_easy_cleanup(curl);

		#ifdef _DEBUG
			std::cout << httpData->c_str() << std::endl;
		#endif

		if (httpCode == 200)
		{
			using jsonss = nlohmann::json;
			auto jsons = jsonss::parse(httpData->c_str());

			/* If the language is not english then return the language */
			if (jsons.at("lang") != "en")
			{
				return jsons.at("lang");
			}
		}

		return "en";
	}

	std::string translate_message(const std::string& text)
	{
		const std::string url = "https://translate.yandex.net/api/v1.5/tr.json/translate";
		const std::string key = "trnsl.1.1.20191022T053454Z.48addd19cae14adf.5e541e8704c8abc207abd615968dcdb2f79ff02d";

		auto _url = url + std::string("?key=") + key;
		_url += std::string("&text=") + urlencode(text);

		/* If the language is english then return nothing so our handler can catch it */
		const std::string lang = get_translation_language(text);

		if (lang != "en")
		{
			_url += std::string("&lang=") + lang + "-en";
		}
		else
		{
			return "";
		}	

		CURL* curl = curl_easy_init();

		// Set remote URL.
		curl_easy_setopt(curl, CURLOPT_URL, _url.c_str());

		// Don't bother trying IPv6, which would increase DNS resolution time.
		curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

		// Don't wait forever, time out after 10 seconds.
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

		// Follow HTTP redirects if necessary.
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

		// Response information.
		int httpCode(0);
		std::unique_ptr<std::string> httpData(new std::string());

		// Hook up data handling function.
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

		// Hook up data container (will be passed as the last parameter to the
		// callback handling function).  Can be any pointer type, since it will
		// internally be passed as a void pointer.
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());

		// Run our HTTP GET command, capture the HTTP response code, and clean up.
		curl_easy_perform(curl);
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
		curl_easy_cleanup(curl);

		#ifdef _DEBUG
			std::cout << httpData->c_str() << std::endl;
		#endif

		/* 200 = successful response from API */
		if (httpCode == 200)
		{
			using json = nlohmann::json;
			auto jsons = json::parse(httpData->c_str());
			return jsons["text"][0].get<std::string>();
		}

		return std::string();
	}

	void handle_translation()
	{
		if (!globals::transmessage.empty())
		{
			/* Check if our translation messages is empty */
			if (!globals::msg_holder.empty())
			{
				/* Loop through meesage array */
				for (size_t i = 0; i < globals::msg_holder.size(); i++)
				{
					/* Check if any of our messages in the array does not match the current translation message */
					if (globals::msg_holder[i] != globals::transmessage)
					{
						/* Translate the incoming message and clear the global message variable */
						std::cout << translator::translate_message(globals::transmessage) << std::endl;
						globals::transmessage.clear();
					}
				}
			}
			else
			{
				/* Since the array has no items in it at first then the first incoming message will be auto translated and will clear the global message variable */
				std::cout << translator::translate_message(globals::transmessage) << std::endl;
				globals::transmessage.clear();
			}

			/* Add each of the incoming messages to the global message array */
			globals::msg_holder.push_back(globals::transmessage);
		}
	}

	void translator_thread()
	{
		while (true)
		{
			std::future<void> fn = std::async(std::launch::async, handle_translation);

			if (fn.valid())
				fn.get();
		}
	}
}
