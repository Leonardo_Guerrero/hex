#pragma once

#include "general_includes.h"
#include "getvfunc.h"
#include "vguipanel_t.h"

typedef unsigned int VPANEL;

class CEngineVGui
{
public:
	VPANEL* GetPanel(VGuiPanel_t type)
	{
		typedef VPANEL* (__thiscall* OriginalFn)(PVOID, VGuiPanel_t);
		return getvfunc<OriginalFn>(this, 1)(this, type);
	}
};