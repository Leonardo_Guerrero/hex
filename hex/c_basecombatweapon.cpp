#include "c_basecombatweapon.h"
#include "globalvars.h"

float C_BaseCombatWeapon::flNextPrimaryAttack() 
{
	return *reinterpret_cast<float*>(reinterpret_cast<DWORD>(this) + globalvars::m_flNextPrimaryAttack);
}