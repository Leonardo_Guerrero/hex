#pragma once

#include "interfaces.h"
#include "dependencies/aliases.hh"
#include "dependencies/definitions.hh"

namespace menu_input
{
	fgui::state get_key_state(const fgui::key& key);
	fgui::delta get_scroll_delta();
	fgui::point get_mouse_position();
	void init();
}