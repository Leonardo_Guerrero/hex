#pragma once


#include "curl/includes/curl.h"

#include "general_includes.h"


namespace
{
	std::size_t callback(
		const char* in,
		std::size_t size,
		std::size_t num,
		std::string* out)
	{
		const std::size_t totalBytes(size * num);
		out->append(in, totalBytes);
		return totalBytes;
	}
}

namespace translator
{
	void hexchar(unsigned char c, unsigned char& hex1, unsigned char& hex2);
	std::string urlencode(std::string s);
	std::string get_translation_language(const std::string& text);
	std::string translate_message(const std::string& text);
	void handle_translation();
	void translator_thread();
}