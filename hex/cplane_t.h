#pragma once

#include "vector.h"

struct cplane_t
{
	Vector normal;
	float dist;
	BYTE type;
	BYTE signbits;
	BYTE pad[2];
};