#include "hacks.h"
#include "math.h"
#include "globals.h"
#include "cbasefilter.h"
#include "fl_states.h"


namespace hacks
{
	bool valid_entity(C_BaseEntity* pEntity)
	{
		if (!pEntity) return false;
		if (pEntity->IsDormant()) return false;
		if (!pEntity->IsAlive()) return false;
		return true;
	}

	namespace demoknight
	{
		void demoknight_attack()
		{
			Vector traceStart, traceEnd;
			Vector viewAngles;
			interfaces::p_engine->GetViewAngles(viewAngles);
			math::trig::AngleVectors(viewAngles, &traceEnd);
			traceStart = globals::m_local->GetEyePosition();
			traceEnd = traceStart + (traceEnd * 8192.0f);
			Ray_t ray; trace_t tr; CBaseFilter filter;
			ray.Init(traceStart, traceEnd);
			filter.pSkip = globals::m_local;
			interfaces::p_enginetrace->TraceRay(ray, 0x46004003, &filter, &tr);
			if (!tr.m_pEnt) return;
			if (tr.m_pEnt->GetClientClass()->chName == nullptr) return;

			/* If our ray hits a player */
			if (_stricmp(tr.m_pEnt->GetClientClass()->chName, "CTFPlayer") == 0)
			{
				/* and our distance to that player is within range */
				if (math::distanceMeters(globals::m_local->GetAbsOrigin(), tr.m_pEnt->GetAbsOrigin()) <= 2.1f)
				{
					/* Weapon nullptr check */
					if (!globals::m_local->GetActiveWeapon()) return;

					float flServerTime = globals::m_local->TickBase() * interfaces::p_globals->m_intervalPerTick;

					if (flServerTime > globals::m_local->GetActiveWeapon()->flNextPrimaryAttack())
						globals::m_cmd->buttons |= IN_ATTACK;
				}
			}
		}
	}
}