#include "log_info.h"

void log_info::draw_log_info()
{
	/* Don't run if the vector is empty. */
	if (!info_list.empty())
	{
		for (size_t i = 0; i < info_list.size(); i++)
		{
			unsigned int timer = static_cast<unsigned int>(interfaces::p_globals->m_curTime - info_list[i].time);
			const int start_fade_time = 15;

			int w, h = 0;
			drawing::get_text_size(drawing::m_espfont, w, h, info_list[i].txt);
			drawing::draw_string(drawing::m_espfont, screen::ScrW / 2 - (w / 2), (screen::ScrH - h) - i * 20, info_list[i].col, info_list[i].txt);

			/* decrease alpha value when start_fade_time is hit */
			if (timer >= start_fade_time)
				info_list[i].col.a *= 0.98;

			/* Remove message once alpha hits 0 */
			if (info_list[i].col.a == 0)
				info_list.erase(info_list.begin(), info_list.begin() + i + 1); // Remove each element.
		}
	}
}

void log_info::add_log_messasge(Color col, const char* message)
{
	info_list.push_back(info(message, interfaces::p_globals->m_curTime, col));
}