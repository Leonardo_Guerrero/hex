#pragma once

#include "c_baseentity.h"
#include "engine_client.h"
#include "engine_vgui.h"
#include "ipanel.h"
#include "isurface.h"
#include "c_global_vars_base.h"
#include "clientmodeshared.h"
#include "ccliententitylist.h"
#include "ienginetrace.h"
#include "cinputsystem.h"

namespace interfaces
{
	extern C_BaseEntity* p_client;
	extern EngineClient* p_engine;
	extern CEngineVGui* p_enginevgui;
	extern IPanel* p_panel;
	extern ISurface* p_surface;
	extern CClientEntityList* p_entitylist;
	extern CGlobalVarsBase* p_globals;
	extern ClientModeShared* p_clientmode;
	extern IEngineTrace* p_enginetrace;
	extern CInputSystem* p_inputsystem;
}