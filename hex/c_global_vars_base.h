#pragma once

class CGlobalVarsBase
{
public:
	float m_realTime;
	int m_frameCount;
	float m_absFrameTime;
	float m_curTime;
	float m_frameTime;
	int m_maxClients;
	int m_tickCount;
	float m_intervalPerTick;
	float m_interpAmount;
};