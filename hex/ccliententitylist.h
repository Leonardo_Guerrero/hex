#pragma once

#include "c_baseentity.h"
#include "getvfunc.h"

class CClientEntityList
{
public:

	C_BaseEntity* GetClientEntity(int Index)
	{
		typedef C_BaseEntity* (__thiscall* Fn)(void*, int);
		return getvfunc<Fn>(this, 3)(this, Index);
	}

	C_BaseEntity* GetClientEntityFromHandle(int Handle)
	{
		typedef C_BaseEntity* (__thiscall* Fn)(void*, int);
		return getvfunc<Fn>(this, 4)(this, Handle);
	}

	int GetHighestEntityIndex()
	{
		typedef int(__thiscall* Fn)(void*);
		return getvfunc<Fn>(this, 6)(this);
	}
};