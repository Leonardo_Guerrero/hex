#pragma once

#include "general_includes.h"
#include "buttoncode_t.h"
#include "getvfunc.h"
#include "analog_code_t.h"

class CInputSystem
{
public:
	bool IsButtonDown(int code)
	{
		typedef bool(__thiscall* OriginalFn)(PVOID, int);
		return getvfunc<OriginalFn>(this, 11)(this, code);
	}

	int GetAnalogDelta(analog_code_t code)
	{
		typedef int(__thiscall* OriginalFn)(PVOID, analog_code_t);
		return getvfunc<OriginalFn>(this, 15)(this, code);
	}
};