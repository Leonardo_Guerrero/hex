#pragma once

#include "isurface.h"

namespace onscreensizechanged
{
	/* Typedef */
	typedef void(__thiscall* OnScreenSizeChangedFn)(ISurface*, int, int);

	/* Original return */
	extern OnScreenSizeChangedFn oOnScreenSizeChanged;

	/* Hooked function */
	void __stdcall Hooked_OnScreenSizeChanged(int oldWidth, int oldHeight);
}