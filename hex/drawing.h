#pragma once

#include "interfaces.h"
#include "isurface.h"
#include "engine_client.h"
#include "screen_size.h"
#include "efontflags.h"
#include "color.h"
#include "dependencies/aliases.hh"
#include "dependencies/definitions.hh"

typedef unsigned long HFont;

namespace drawing
{
	extern HFont m_menufont;
	extern HFont m_espfont;

	extern void init();
	extern void draw_string(HFont font, int x, int y, Color color, const char* text, ...);
	
	extern void draw_rect(int x, int y, int x2, int y2, Color c);
	extern void draw_outlined_rectangle(int x, int y, int x2, int y2, Color c);
	extern void draw_line(int x, int y, int x2, int y2, Color c);
	extern void get_text_size(HFont font, int& w, int& h, const char* message, ...);

	namespace menu
	{
		extern void create_font(fgui::font& font, const std::string_view family, int size, int flags, bool bold);
		extern void get_screen_size(int& width, int& height);
		extern fgui::dimension get_text_size(const fgui::font& font, const std::string_view text);
		extern void draw_rect(int x, int y, int width, int height, fgui::color color);
		extern void draw_outlined_rectangle(int x, int y, int width, int height, fgui::color color);
		extern void draw_line(int x, int y, int x2, int y2, fgui::color color);
		extern void draw_string(int x, int y, fgui::color color, fgui::font font, const std::string_view text);
		extern void draw_colored_gradient(int x, int y, int width, int height, fgui::color color1, fgui::color color2, bool is_horizontal);
		extern void draw_clip_rect(int x, int y, int width, int height);
	}
};