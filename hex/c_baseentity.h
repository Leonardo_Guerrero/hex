#pragma once

#include "general_includes.h"
#include "getvfunc.h"
#include "clientclass.h"
#include "vector.h"
#include "c_basecombatweapon.h"

class C_BaseEntity
{
public:

	template<typename T>
	T get(DWORD off)
	{
		return *(T*)((DWORD)this + off);
	}
	template<typename T>
	void set(DWORD off, T val)
	{
		(*(T*)((DWORD)this + off)) = val;
		return;
	}

	ClientClass* GetClientClass()
	{
		PVOID pNetworkable = static_cast<PVOID>(this + 0x8);
		typedef ClientClass* (__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(pNetworkable, 2)(pNetworkable);
	}

	ClientClass* GetAllClasses()
	{
		typedef ClientClass* (__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 8)(this);
	}

	Vector& GetAbsOrigin()
	{
		typedef Vector& (__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 9)(this);
	}

	bool IsDormant()
	{
		PVOID pNetworkable = (PVOID)(this + 0x8);
		typedef bool(__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(pNetworkable, 8)(pNetworkable);
	}

	int GetHealth()
	{
		typedef int(__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 106)(this);
	}

	int GetFlags();
	BYTE GetLifeState();
	bool IsAlive();
	Vector VecViewOffset();
	Vector GetEyePosition();
	Vector GetOrigin();
	C_BaseCombatWeapon* GetActiveWeapon();
	int TickBase();
};