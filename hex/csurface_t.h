#pragma once

struct csurface_t
{
	const char* name;
	short surfaceProps;
	unsigned short flags;
};