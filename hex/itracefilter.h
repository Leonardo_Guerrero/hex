#pragma once

#include "c_baseentity.h"
#include "tracetype_t.h"

class ITraceFilter
{
public:
	virtual bool ShouldHitEntity(C_BaseEntity* pEntity, int contentsMask)
	{
		return !(pEntity == pSkip);
	}
	virtual TraceType_t GetTraceType()
	{
		return TRACE_EVERYTHING;
	}
	void* pSkip;
};