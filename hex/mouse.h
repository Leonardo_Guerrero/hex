#pragma once

enum mouse
{
	PITCH = 0, // up / down
	YAW, // left / right
	ROLL // fall over
};