#include "interfaces.h"
#include "get_pointer.h"
#include "utils.h"

namespace interfaces
{
	C_BaseEntity* p_client = static_cast<C_BaseEntity*>(get_pointer::GetPointer("client.dll", "VClient"));
	EngineClient* p_engine = static_cast<EngineClient*>(get_pointer::GetPointer("engine.dll", "VEngineClient"));
	CEngineVGui* p_enginevgui = static_cast<CEngineVGui*>(get_pointer::GetPointer("engine.dll", "VEngineVGui"));
	IPanel* p_panel = static_cast<IPanel*>(get_pointer::GetPointer("vgui2.dll", "VGUI_Panel"));
	ISurface* p_surface = static_cast<ISurface*>(get_pointer::GetPointer("vguimatsurface.dll", "VGUI_Surface"));
	CClientEntityList* p_entitylist = static_cast<CClientEntityList*>(get_pointer::GetPointer("client.dll", "VClientEntityList"));
	CGlobalVarsBase* p_globals = *reinterpret_cast<CGlobalVarsBase**>(Utils::FindSignature("engine.dll", "A1 ? ? ? ? 8B 11 68") + 8);
	ClientModeShared* p_clientmode = **(ClientModeShared***)((*(DWORD**)p_client)[10] + 0x5);
	IEngineTrace* p_enginetrace = static_cast<IEngineTrace*>(get_pointer::GetPointer("engine.dll", "EngineTraceClient"));
	CInputSystem* p_inputsystem = static_cast<CInputSystem*>(get_pointer::GetPointer("inputsystem.dll", "InputSystemVersion"));
}