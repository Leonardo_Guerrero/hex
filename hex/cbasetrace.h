#pragma once

#include "vector.h"
#include "cplane_t.h"

class CBaseTrace
{
public:
	Vector start;
	Vector end;
	cplane_t plane;
	float fraction;
	int contents;
	unsigned short dispFlags;
	bool allsolid;
	bool startsolid;
};