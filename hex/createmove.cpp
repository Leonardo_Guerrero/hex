#include "createmove.h"
#include "interfaces.h"
#include "log_info.h"
#include "globals.h"
#include "fl_states.h"
#include "hacks.h"


namespace createmove
{
	CreateMoveFn oCreateMove;

	bool __stdcall Hooked_CreateMove(float flInputSampleTime, CUserCmd* UserCmd)
	{
		oCreateMove(flInputSampleTime, UserCmd);

		if (!UserCmd->command_number)
			return false;

		/* Grab cmd when createmove is running */
		globals::m_cmd = UserCmd;

		if (interfaces::p_engine->IsInGame() && interfaces::p_engine->IsConnected())
		{
			
			if (UserCmd->buttons & IN_JUMP) {
				if (!(globals::m_local->GetFlags() & FL_ONGROUND))
					UserCmd->buttons &= ~IN_JUMP;
			}

			hacks::demoknight::demoknight_attack();

			if (interfaces::p_inputsystem->IsButtonDown(KEY_DOWN))
			{
				std::cout << "ActiveWeapon: " << std::hex << globals::m_local->GetActiveWeapon() << std::endl;
			}
				
		}

		return false;
	}
}