#pragma once

#include "vtable_hook.h"
#include "interfaces.h"

namespace hooks
{
	void init();
	void remove(HMODULE dll);
}