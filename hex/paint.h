#pragma once

#include "general_includes.h"
#include "paintmode_t.h"

namespace paint
{
	/* Typedef */
	typedef void* (__fastcall* PaintFn)(PVOID, PVOID, PaintMode_t);

	/* Original return */
	extern PaintFn oPaint;

	/* Hooked function */
	void __fastcall Hooked_Paint(PVOID ecx, PVOID edx, PaintMode_t mode);
}