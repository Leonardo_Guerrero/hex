#pragma once

#include "general_includes.h"
#include "getvfunc.h"

class ISurface
{
public:
	void DrawSetColor(int r, int g, int b, int a)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this, 11)(this, r, g, b, a);
	}
	void DrawFilledRect(int x0, int y0, int x1, int y1)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this, 12)(this, x0, y0, x1, y1);
	}
	void DrawOutlinedRect(int x0, int y0, int x1, int y1)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this, 14)(this, x0, y0, x1, y1);
	}
	void DrawOutlinedCircle(int x, int y, int radius, int segments)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		return getvfunc<OriginalFn>(this, 99)(this, x, y, radius, segments);
	}
	void DrawLine(int x0, int y0, int x1, int y1)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this, 15)(this, x0, y0, x1, y1);
	}
	void DrawPolyLine(int* x, int* y, int n)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int*, int*, int);
		getvfunc<OriginalFn>(this, 16)(this, x, y, n);
	}

	void DrawSetTextFont(unsigned long font)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, unsigned long);
		getvfunc<OriginalFn>(this, 17)(this, font);
	}
	void DrawSetTextColor(int r, int g, int b, int a)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this, 19)(this, r, g, b, a);
	}
	void DrawSetTextPos(int x, int y)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int);
		getvfunc<OriginalFn>(this, 20)(this, x, y);
	}
	void DrawPrintText(const wchar_t* text, int textLen)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, const wchar_t*, int, int);
		return getvfunc<OriginalFn>(this, 22)(this, text, textLen, 0);
	}
	unsigned long CreateFont()
	{
		typedef unsigned int(__thiscall* OriginalFn)(PVOID);
		return getvfunc<OriginalFn>(this, 66)(this);
	}
	void SetFontGlyphSet(unsigned long& font, const char* windowsFontName, int tall, int weight, int blur, int scanlines, int flags)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, unsigned long, const char*, int, int, int, int, int, int, int);
		getvfunc<OriginalFn>(this, 67)(this, font, windowsFontName, tall, weight, blur, scanlines, flags, 0, 0);
	}
	void GetTextSize(unsigned long font, const wchar_t* text, int& wide, int& tall)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, unsigned long, const wchar_t*, int&, int&);
		getvfunc<OriginalFn>(this, 75)(this, font, text, wide, tall);
	}

	void PlaySound(const char* sound)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, const char*);
		return getvfunc<OriginalFn>(this, 78)(this, sound);
	}
	void UnlockCursor()
	{
		return getvfunc<void(__thiscall*)(PVOID)>(this, 61)(this);
	}
	void LockCursor()
	{
		return getvfunc<void(__thiscall*)(PVOID)>(this, 62)(this);
	}

	void SetCursorAlwaysVisible(bool vis)
	{
		return getvfunc<void(__thiscall*)(PVOID, bool)>(this, 52)(this, vis);
	}
	void SurfaceGetCursorPos(int& x, int& y)
	{
		return getvfunc<void(__thiscall*)(PVOID, int&, int&)>(this, 96)(this, x, y);
	}

	bool IsCursorLocked()
	{
		return getvfunc<bool(__thiscall*)(PVOID)>(this, 104)(this);
	}

	void DrawFilledRectFade(int x0, int y0, int x1, int y1, unsigned int alpha0, unsigned int alpha1, bool bHorizontal)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int, size_t, size_t, bool);
		getvfunc<OriginalFn>(this, 118)(this, x0, y0, x1, y1, alpha0, alpha1, bHorizontal);
	}

	void SetSoftwareCursor(bool t)
	{
		return getvfunc<void(__thiscall*)(PVOID, bool)>(this, 130)(this, t);
	}

	//maybe 143?
	void SetClippingRect(int x, int y, int w, int h)
	{
		typedef void(__thiscall* OriginalFn)(PVOID, int, int, int, int);
		getvfunc<OriginalFn>(this, 158)(this, x, y, w, h);
	}
};