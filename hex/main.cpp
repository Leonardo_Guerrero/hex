#include "interfaces.h"
#include "hooks.h"
#include "drawing.h"
#include "globalvars.h"
#include "translator.h"
#include "menu_input.h"
#include "menu.h"

HMODULE dll = NULL;

unsigned long __stdcall ThreadEntry(void* param)
{
	#ifdef _DEBUG
		AllocConsole();
		freopen("CONOUT$", "w", stdout);
	#endif

	/* Initialize our drawing stuff */
	drawing::init();

	/* Initialize our menu inputs */
	menu_input::init();

	/* Render our menu */
	c_menu::render();

	/* Initialize all our hooks */
	hooks::init();

	/* Initialize our offsets */
	globalvars::init();

	/* Initialize our translation thread to run asynchronously */
	translator::translator_thread();

	/* Unhook everything */
	hooks::remove(dll);

	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD reason, LPVOID reserved)
{
	switch (reason) {
	case DLL_PROCESS_ATTACH:
		dll = hDll;
		CreateThread(nullptr, NULL, reinterpret_cast<LPTHREAD_START_ROUTINE>(ThreadEntry), nullptr, NULL, nullptr);
		return TRUE;
		break;

	case DLL_PROCESS_DETACH:
		return TRUE;
		break;
	}

	return FALSE;
}
