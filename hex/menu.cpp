#include "menu.h"


namespace c_menu
{

	void render()
	{
		fgui::element_font title_font = { "Tahoma", 11, fgui::external::font_flags::SHADOW , false };

		REGISTER_NOTIFICATIONS(0, 0, title_font);
		REGISTER_CURSOR(fgui::cursor_type::ARROW, fgui::input_state::UNLOCKED);

		ADD_WINDOW(container["#window"], 50, 50, "hex", 560, 450, fgui::external::key_code::KEY_INSERT, title_font, false);
		{
			REGISTER_TAB(tabs["#tab_panel"], 7, 1, fgui::tab_layout::HORIZONTAL, title_font, container["#window"], -1);

			ADD_TAB(tabs["#tab_panel"], "Aimbot");
			{
				ADD_GROUPBOX(container["#groupbox"], 15, 40, "Aimbot Options", 360, 370, title_font, container["#window"], 0, false, false, false);
				ADD_CONTROLLER(container["#groupbox"], tabs["#tab_panel"]);

				ADD_CHECKBOX(checkbox["#aimbot"], 10, 10, "Aimbot", "vars.aimbot", title_font, container["#groupbox"], 0);
				ADD_CHECKBOX(checkbox["#ragebot"], 10, 30, "Ragebot", "vars.ragebot", title_font, container["#groupbox"], 0);
				ADD_CHECKBOX(checkbox["#silentaim"], 10, 50, "Silent Aim", "vars.silentaim", title_font, container["#groupbox"], 0);
				ADD_SLIDER(slider["#aimbotfov"], 10, 85, "Aimbot FOV", 15, 0, 180, "vars.aimbotfov", title_font, container["#groupbox"], 0);
				ADD_CHECKBOX(checkbox["#ignorefriends"], 10, 100, "Ignore Friends", "vars.ignorefriends", title_font, container["#groupbox"], 0);
				ADD_CHECKBOX(checkbox["#ignoreteam"], 10, 120, "Ignore Team", "vars.ignoreteam", title_font, container["#groupbox"], 0);
				ADD_CHECKBOX(checkbox["#ignoreadmins"], 10, 140, "Ignore Admins", "vars.ignoreadmins", title_font, container["#groupbox"], 0);
				ADD_CHECKBOX(checkbox["#nospread"], 10, 160, "Nospread", "vars.nospread", title_font, container["#groupbox"], 0);
				ADD_CHECKBOX(checkbox["#nospread0x7"], 10, 180, "Nospread-Type -> 0x7FFFFFFF", "vars.silentaim", title_font, container["#groupbox"], 0);
			}

			ADD_TAB(tabs["#tab_panel"], "Visuals");
			{

			}

			ADD_TAB(tabs["#tab_panel"], "Misc");
			{
				ADD_GROUPBOX(container["#groupbox"], 15, 40, "Misc Options", 360, 370, title_font, container["#window"], 2, false, false, false);
				ADD_CONTROLLER(container["#groupbox"], tabs["#tab_panel"]);

				ADD_BUTTON(button["#addons"], 10, 10, "Addons", 50, 25, title_font, container["#groupbox"], 2)
				ADD_CHECKBOX(checkbox["#nohands"], 10, 45, "No Hands", "vars.nohands", title_font, container["#groupbox"], 2);
				ADD_CHECKBOX(checkbox["#antiscreengrab"], 10, 65, "Anti-Screengrab", "vars.antiscreengrab", title_font, container["#groupbox"], 2);
				ADD_CHECKBOX(checkbox["#playerlist"], 10, 85, "Playerlist", "vars.playerlist", title_font, container["#groupbox"], 2);
				ADD_SLIDER(slider["#vmfov"], 10, 120, "VM FOV", 90, 0, 180, "vars.vmfov", title_font, container["#groupbox"], 2);
				ADD_CHECKBOX(checkbox["#thirdperson"], 10, 140, "Thirdperson", "vars.thirdperson", title_font, container["#groupbox"], 2);
				ADD_CHECKBOX(checkbox["#bunnyhop"], 10, 160, "Bunnyhop", "vars.bunnyhop", title_font, container["#groupbox"], 2);
				ADD_CHECKBOX(checkbox["#addmarkers"], 10, 180, "Add Markers", "vars.addmarkers", title_font, container["#groupbox"], 2);
				ADD_CHECKBOX(checkbox["#walkbot"], 10, 200, "Walkbot", "vars.walkbot", title_font, container["#groupbox"], 2);
				ADD_CHECKBOX(checkbox["#fakelag"], 10, 220, "Fakelag", "vars.fakelag", title_font, container["#groupbox"], 2);
				ADD_SLIDER(slider["#fakelagticks"], 10, 255, "Fakelag-Ticks", 14, 0, 28, "vars.vmfov", title_font, container["#groupbox"], 2);
				ADD_CHECKBOX(checkbox["#antiaim"], 10, 275, "Anti-Aim", "vars.antiaim", title_font, container["#groupbox"], 2);
			}
		}

	}
}