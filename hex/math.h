#pragma once

#include "vector.h"

namespace math
{
	float distanceMeters(Vector v1, Vector v2);

	namespace matrix
	{
		bool WorldToScreen(Vector& vOrigin, Vector& vScreen);
	}

	namespace trig
	{
		void SinCos(float radians, float* sine, float* cosine);
		void AngleVectors(const Vector& angles, Vector* forward);
	}
}