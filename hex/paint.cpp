#include "paint.h"
#include "utils.h"
#include "interfaces.h"
#include "drawing.h"
#include "globals.h"

#include "FGUI.hh"
#include "menu.h"

namespace paint
{
	PaintFn oPaint;

	static auto StartDrawing = reinterpret_cast<void(__thiscall*)(void*)>(Utils::FindSignature("vguimatsurface.dll", "55 8B EC 64 A1 ? ? ? ? 6A FF 68 ? ? ? ? 50 64 89 25 ? ? ? ? 83 EC 14"));
	static auto FinishDrawing = reinterpret_cast<void(__thiscall*)(void*)>(Utils::FindSignature("vguimatsurface.dll", "55 8B EC 6A FF 68 ? ? ? ? 64 A1 ? ? ? ? 50 64 89 25 ? ? ? ? 51 56 6A 00"));

	void __fastcall Hooked_Paint(PVOID ecx, PVOID edx, PaintMode_t mode)
	{

		oPaint(ecx, edx, mode);

		if (mode & PAINT_UIPANELS)
		{
			StartDrawing(interfaces::p_surface);

			/* Draw all logs that will appear later */
			globals::log->draw_log_info();

			/* FGUI render handler */
			fgui::handler::render_window();

			if (interfaces::p_engine->IsInGame() && interfaces::p_engine->IsConnected())
			{
				/*	Grab our localplayer and store it in our global pointer

					NOTE: I left it in paint because it runs before any other hook
					and updates the pointer before anything else which gets rid of
					nullptr errors from reading from null memory.
				*/
				globals::m_local = reinterpret_cast<C_BaseEntity*>(interfaces::p_entitylist->GetClientEntity(interfaces::p_engine->GetLocalPlayer()));

			}
			FinishDrawing(interfaces::p_surface);
		}
	}
}