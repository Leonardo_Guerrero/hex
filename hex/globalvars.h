#pragma once

#include "general_includes.h"

namespace globalvars
{
	extern DWORD m_fFlags;
	extern DWORD m_lifeState;
	extern DWORD m_vecViewOffset;
	extern DWORD m_vecOrigin;
	extern DWORD m_hActiveWeapon;
	extern DWORD m_flNextPrimaryAttack;
	extern DWORD m_nTickBase;

	void init();
}