#include "c_baseentity.h"
#include "globalvars.h"
#include "fl_states.h"
#include "interfaces.h"

int C_BaseEntity::GetFlags()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + globalvars::m_fFlags);
}

BYTE C_BaseEntity::GetLifeState()
{
	return *reinterpret_cast<BYTE*>(reinterpret_cast<DWORD>(this) + globalvars::m_lifeState);
}

bool C_BaseEntity::IsAlive()
{
	return (GetLifeState() != LIFE_DEAD);
}

Vector C_BaseEntity::VecViewOffset() {
	return *reinterpret_cast<Vector*>(reinterpret_cast<DWORD>(this) + globalvars::m_vecViewOffset);
}

Vector C_BaseEntity::GetEyePosition() {
	return GetAbsOrigin() + VecViewOffset();
}

Vector C_BaseEntity::GetOrigin() {
	return *reinterpret_cast<Vector*>(reinterpret_cast<DWORD>(this) + globalvars::m_vecOrigin);
}

C_BaseCombatWeapon* C_BaseEntity::GetActiveWeapon() {
	int hActiveWeapon = *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + globalvars::m_hActiveWeapon);
	if (!hActiveWeapon)
		return NULL;

	return (C_BaseCombatWeapon*)interfaces::p_entitylist->GetClientEntityFromHandle(hActiveWeapon);
}

int C_BaseEntity::TickBase()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + globalvars::m_nTickBase);
}